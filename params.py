import os
from os import path as op
import sys
home = os.getenv("USERPROFILE") if sys.platform=='win32' else os.getenv("HOME")

# Path to directory containing features file
session_path = op.join(home, "Documents", "Data", "spike_sorting", "easy1", "arseny")

# Cutoff settings
# Percentile of distances between event pairs in feature space used to estimate per-site cutoff.
dc_percentile = 2.
# Maximum number of iterations over feature subsets to estimate per-site cutoff distance.
dc_iters = 100
# Relative error between iterations of estimated cutoff distance before convergence.
dc_tol = 1e-5
# Minimum log10(rho) value to satisfy being a cluster center.
rho_cutoff = -3
# Minimum log10(delta) value to satisfy being a cluster center.
delta_cutoff = 0.75

# Detrending settings:
# Use detrending to find cluster centers if True.
detrend = True
# Use quadratic detrending if True, else linear.
detrend_quadratic = True
# Detrend on all sites if True, else on a per-site basis.
detrend_global = True

# Maximum number of iterations over unassigned clusters (steps down density gradient) in assignment.
assign_iters = 100

# Output filename.
outfile = "cluster_results.csv"
