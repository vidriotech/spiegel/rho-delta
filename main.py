#!/usr/bin/env python3

from . import params as p
from .session import load_jrc, load_jrc_features
from .cluster import rho_delta_all, cluster_centers, assign_clusters


def main():
    session_data, dc_percentile = load_jrc(p.session_path)
    features = load_jrc_features(p.session_path)

    table = rho_delta_all(session_data, features, p.subset, dc_percentile, p.dc_iters)
    cluster_centers(table, rho_cutoff=p.rho_cutoff, delta_cutoff=p.delta_cutoff, detrend=p.detrend,
                    detrend_global=p.detrend_global, quadratic=p.detrend_quadratic)
    assign_clusters(table, iters=p.assign_iters)

    # write results to CSV
    table.to_csv(p.outfile, index=False)


if __name__ == "__main__":
    main()
