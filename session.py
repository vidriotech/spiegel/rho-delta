from collections import OrderedDict
import os
from os import path as op

import h5py
import numpy as np
import pandas as pd
import scipy.io


def _read_matlab(filename, field, flatten=False):
    """Read from a Matlab .mat file.

    Parameters
    ----------
    filename : str
        Path to .mat file to read.
    field : str
        Field to read from MAT file.

    Returns
    -------
    result : numpy.ndarray
    """

    assert op.isfile(filename)

    try:
        matfile = scipy.io.loadmat(filename)
    except NotImplementedError:
        matfile = h5py.File(filename, 'r')

    if isinstance(matfile, dict):  # old-style MAT file
        result = matfile[field]
    else:
        result = matfile.get(field).value
        matfile.close()

    if flatten:
        result = result.ravel()
    else:
        result = result.T

    return result


def _read_jrc(filename, dims, dtype):
    """Read from a JRCLUST .jrc file.

    Parameters
    ----------
    filename : str
        Path to file containing data.
    dims : iterable
        Dimensions of data in file.
    dtype : type
        NumPy data type of data in file.

    Returns
    -------
    result : numpy.ndarray
    """

    assert op.isfile(filename)
    assert isinstance(dtype, type)
    assert len(dims) == 3

    # `shape` as a parameter to memmap fails here
    result = np.fromfile(filename, dtype=dtype).reshape(*dims, order='F')

    return result


def _jrc_prefix(dirname):
    """Get a JRCLUST session prefix.

    Parameters
    ----------
    dirname : str
        Path to directory containing JRCLUST output.

    Returns
    -------
    prefix : str
        Session name for JRCLUST sorting.

    Raises
    ------
    ValueError
        If more than one distinct session is found in directory.

    """

    assert op.isdir(dirname)
    ls = os.listdir(dirname)

    matfiles = [f for f in ls if f.endswith("_jrc.mat")]
    assert len(matfiles) > 0

    if len(matfiles) > 1:  # more than one file ending in _jrc.mat -- which one do we take?
        raise ValueError(f"ambiguous data directory: {dirname}")

    return matfiles[0][:-8]


def load_jrc(dirname, consolidate=True):
    """Load a JRCLUST sorting.

    Parameters
    ----------
    dirname : str
        Path to directory containing JRCLUST output (*_jrc.mat, *.jrc).
    consolidate : bool
        Consolidate all negative cluster IDs into -1 if True.

    Returns
    -------
    event_annotations : pandas.DataFrame
        Zero-based timesteps, integer cluster IDs, zero-based channel indices.
    """

    prefix = _jrc_prefix(dirname)  # handles assertions for us
    filename = op.join(dirname, f"{prefix}_jrc.mat")

    # times and channel indices are zero-based since they serve as indices into data
    event_times = _read_matlab(filename, "viTime_spk", flatten=True).astype(np.int64) - 1
    primary_site = _read_matlab(filename, "viSite_spk", flatten=True).astype(np.int64) - 1
    secondary_site = _read_matlab(filename, "viSite2_spk", flatten=True).astype(np.int64) - 1

    # cluster IDs are not indices (and can in fact be negative)
    event_clusters = _read_matlab(filename, "S_clu/viClu_auto", flatten=True)

    # get JRC's rho, delta, dc, nearest neighbor
    rho = _read_matlab(filename, "S_clu/rho", flatten=True)
    delta = _read_matlab(filename, "S_clu/delta", flatten=True)
    nearest_neighbor = _read_matlab(filename, "S_clu/nneigh", flatten=True) - 1

    dc_percentile = _read_matlab(filename, "P/dc_percent", flatten=True)[0]

    # sanity check
    assert (event_times == np.sort(event_times)).all()
    assert event_times[0] >= 0
    assert event_times.shape == event_clusters.shape

    ea = pd.DataFrame(data=OrderedDict([("timestep", event_times),
                                        ("primary_site", primary_site),
                                        ("secondary_site", secondary_site),
                                        ("jrc_rho", rho),
                                        ("jrc_delta", delta),
                                        ("jrc_nearest_neighbor", nearest_neighbor),
                                        ("auto_cluster", event_clusters)]))

    # consolidate garbage clusters
    if consolidate:
        ea.loc[ea[ea.auto_cluster < 0].index, "auto_cluster"] = -1

    return ea, dc_percentile


def load_jrc_features(dirname):
    """Load features output by JRCLUST.

    Parameters
    ----------
    dirname : str
        Path to directory containing JRCLUST output (*_jrc.mat, *.jrc).

    Returns
    -------
    result : numpy.ndarray
        Event features.
    """

    prefix = _jrc_prefix(dirname)  # handles assertions for us
    filename = op.join(dirname, f"{prefix}_spkfet.jrc")
    dims = tuple(_read_matlab(op.join(dirname, f"{prefix}_jrc.mat"), "dimm_fet", flatten=True).astype(np.int32))
    dtype = np.float32

    return _read_jrc(filename, dims, dtype)
