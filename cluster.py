import numbers
import warnings

import numpy as np
import scipy.signal
import scipy.spatial
import pandas as pd


# Pairwise distances for this many spikes will take up ~100 MiB in memory
SPIKE_CUTOFF = 5000


def estimate_rho_cutoff(table, site, features, percentile=2.0, iters=50, tol=1e-6):
    r"""Estimate the cutoff value for computing $\rho$.

    Parameters
    ----------
    features : numpy.ndarray
        Matrix, rows are feature vectors.
    percentile : float, optional
        The percentile at which to cut off rho.
    iters : int, optional
        Number of times to sample the the feature vectors.
    tol : float, optional
        Convergence tolerance.

    Returns
    -------
    dist_est : float
        Estimate of distance near the `percentile` cutoff.
    rel_err : float
        The relative change in mean distance from the previous iteration.
    """

    assert isinstance(iters, numbers.Integral) and iters > 0

    site_mask = (table.primary_site == site) | (table.secondary_site == site)
    event_index = table[site_mask].index
    # take row 0 if primary site, row 1 if secondary site
    site_index = (table.loc[event_index].secondary_site == site).astype(np.int32)

    subfeatures = features[:, site_index, event_index].T

    if subfeatures.shape[0] < SPIKE_CUTOFF:  # just take all features if possible
        dists = scipy.spatial.distance.pdist(subfeatures, "euclidean")
        dist_est = np.percentile(dists, percentile)
        rel_err = 0
    else:
        pers = []
        for i in range(iters):
            indices = np.random.choice(np.arange(subfeatures.shape[0]), SPIKE_CUTOFF, replace=False)
            subsubfeatures = subfeatures[indices, :]
            dists = scipy.spatial.distance.pdist(subsubfeatures, "euclidean")

            pers.append(np.percentile(dists, percentile))

            if i == 0:
                prev_est = 0
            else:
                prev_est = dist_est
            dist_est = np.mean(pers)

            # compute the relative change in mean from the previous iteration
            rel_err = (np.abs(dist_est - prev_est)) / dist_est
            if rel_err < tol:
                break

        if rel_err >= tol:
            warnings.warn("rho cutoff may not have converged")

    return dist_est, rel_err


def rho_site(table, site, features, cutoff):
    r"""Compute $\rho$ (local density) for each spike on a given primary site.

    Parameters
    ----------
    table : pandas.DataFrame
        Main data table.
    site : int
        Site of interest for computing rho.
    features : numpy.ndarray
        Matrix, columns are feature vectors.
    cutoff : float
        The distance threshold within which neighbors are counted.
    """

    site_mask = (table.primary_site == site) | (table.secondary_site == site)
    event_index = table[site_mask].index
    # take row 0 if primary site, row 1 if secondary site
    site_index = (table.loc[event_index].secondary_site == site).astype(np.int32)

    subfeatures = features[:, site_index, event_index].T
    rho_vals = np.zeros(np.count_nonzero(site_mask))

    for i in range(rho_vals.size):
        f = subfeatures[i, :][np.newaxis, :]
        g = subfeatures[i + 1:, :]

        dists = scipy.spatial.distance.cdist(f, g).ravel()
        dmask = dists < cutoff

        rho_vals[i] += np.count_nonzero(dmask)

        # since we don't search behind (to save space and time), increment rho within
        # cutoff if we did search behind
        if dmask.any():
            wdists = np.where(dmask)[0] + i + 1
            rho_vals[wdists] += 1

    for i, event_id in enumerate(event_index):
        if table.loc[event_id, "primary_site"] == site:
            table.loc[event_id, "computed_rho"] = rho_vals[i]

    table.loc[table.primary_site == site, "computed_rho"] /= np.count_nonzero(site_mask)


def delta_site(table, site, features):
    r"""Compute $\delta$ (distance separation) for each spike on a given primary site.

    Parameters
    ----------
    table : pandas.DataFrame
        Main data table.
    site : int
        Site of interest for computing rho.
    features : numpy.ndarray
        Matrix, columns are feature vectors.
    """

    assert isinstance(table, pd.DataFrame)

    primary_mask = table.primary_site == site
    site_mask = primary_mask | (table.secondary_site == site)
    event_index = table[site_mask].index
    # take row 0 if primary site, row 1 if secondary site
    site_index = (table.loc[event_index].secondary_site == site).astype(np.int32)

    subfeatures = features[:, site_index, event_index].T  # primary | secondary (n)
    rho_vals = table.loc[site_mask, "computed_rho"]

    assert subfeatures.shape[0] == rho_vals.size

    for i, event_id in enumerate(event_index):
        if table.loc[event_id, "primary_site"] == site:
            rh = table.loc[event_id, "computed_rho"]
            current_feature = subfeatures[i, :][np.newaxis, :]
            denser = rho_vals > rh  # mask

            if denser.any():
                denser_features = subfeatures[denser, :]  # num_denser_events x num_features
                dists = scipy.spatial.distance.cdist(current_feature, denser_features, "euclidean").ravel()
                mp = dists.argmin()

                table.loc[event_id, "computed_delta"] = dists[mp]
                table.loc[event_id, "computed_nearest_neighbor"] = table.loc[site_mask].loc[denser].index[mp] #np.where(denser)[0][mp]
            else:
                other_features = np.vstack((subfeatures[:i, :], subfeatures[i + 1:, :]))

                dists = scipy.spatial.distance.cdist(current_feature, other_features, "euclidean").ravel()
                mp = dists.argmax()

                table.loc[event_id, "computed_delta"] = dists[mp]
                table.loc[event_id, "computed_nearest_neighbor"] = -1


def rho_delta_all(session_data, features, dc_percentile=2., dc_iters=100, dc_tol=1e-5):
    r"""Compute rho, delta, and nearest neighbors for all sites.

    Parameters
    ----------
    session_data : pandas.DataFrame
        Table of timesteps, cluster IDs, primary and secondary sites.
    features : numpy.ndarray
        Tensor (spikes x sites x features), primary and secondary feature vectors per event.
    dc_percentile : float, optional
        Percentile of distances between event pairs in feature space used to estimate per-site cutoff.
    dc_iters : int, optional
        Maximum number of iterations over feature subsets to estimate per-site cutoff distance.
    dc_tol : float, optional
        Relative error between iterations of estimated cutoff distance before convergence.

    Returns
    -------
    table : pandas.DataFrame
        Table of event ids, primary and secondary sites, rho & delta values, and nearest neighbors.
    """

    all_sites = session_data.primary_site.unique()

    table = session_data.copy()
    table["computed_rho"] = np.zeros(table.shape[0])
    table["computed_delta"] = np.zeros(table.shape[0])
    table["computed_nearest_neighbor"] = -np.ones(table.shape[0], dtype=np.int32)
    table["computed_cluster"] = -np.ones(table.shape[0], dtype=np.int32)

    for i, site in enumerate(all_sites):  # rho step
        print(f"Computing rho for site {site} ({i + 1}/{all_sites.size})...", end="")

        # estimate distance cutoff
        dc, err = estimate_rho_cutoff(table=table, site=site, features=features, percentile=dc_percentile,
                                      iters=dc_iters, tol=dc_tol)

        # compute rho
        rho_site(table=table, site=site, features=features, cutoff=dc)

        print("done!")

    for i, site in enumerate(all_sites):  # delta step
        print(f"Computing delta for site {site} ({i + 1}/{all_sites.size})...", end="")

        # compute delta and find nearest neighbors
        delta_site(table=table, site=site, features=features)
        table.loc[table.primary_site == site, "computed_delta"] /= dc

        print("done!")

    return table


def cluster_centers(table, rho_cutoff, delta_cutoff, detrend=False, detrend_global=False, quadratic=False):
    r"""

    Parameters
    ----------
    table : pandas.DataFrame
        Table of event ids, primary and secondary sites, rho & delta values, and nearest neighbors.
    rho_cutoff : float
        Minimum log10(rho) value to satisfy being a cluster center.
    delta_cutoff : float
        Minimum log10(delta) value to satisfy being a cluster center.
    detrend: bool, optional
        Whether to detrend.
    detrend_global : bool, optional
        Will use all sites in detrending if True, otherwise detrend per site.
    quadratic : bool, optional
        Use a quadratic model to detrend if True, else use linear detrending.

    Returns
    -------
    rho : numpy.ndarray
        Subset of rho used to find cluster centers.
    delta : numpy.ndarray
        Subset of delta used to find cluster centers.
    """

    def do_detrend(x, y, quad):
        if quad:  # quadratic detrending
            model = np.polyfit(x, y, 2)
            y_hat = np.polyval(model, x)

            y = y - y_hat
        else:  # linear detrending
            y = scipy.signal.detrend(y)

        return (y - np.mean(y)) / np.std(y)

    sites = np.unique(table.primary_site)

    if detrend and detrend_global:  # do detrending on all sites
        all_rho = table.computed_rho
        all_delta = table.computed_delta

        thresh = (10**rho_cutoff < all_rho)  # threshold rho
        thresh = thresh & (0 < all_delta)  # threshold delta

        r = np.log10(all_rho[thresh])
        d = do_detrend(r, all_delta[thresh], quadratic)

        keep = table.index[thresh][d > 10**delta_cutoff]
    elif detrend:  # detrend on a site-by-site basis
        keep = pd.Int64Index([])
        for site_index in sites:
            site_table = table[table.primary_site == site_index]
            site_rho = site_table.computed_rho
            site_delta = site_table.computed_delta

            site_thresh = (10**rho_cutoff < site_rho) & (site_rho < 0.1)  # threshold rho
            site_thresh = site_thresh & (0 < site_delta) & (site_delta < 1)  # threshold delta

            r = np.log10(site_rho[site_thresh])
            d = do_detrend(r, site_delta[site_thresh], quadratic)

            keep = keep.append(site_table.index[site_thresh][d > 10**delta_cutoff])
    else:  # don't detrend at all
        r, d = table.computed_rho.values, table.computed_delta.values
        keep = table[(table.computed_rho > 10**rho_cutoff) & (table.computed_delta > 10**delta_cutoff)].index

    table.loc[keep, "computed_cluster"] = 1 + np.arange(keep.size)

    return r, d


def assign_clusters(table, iters=1000):
    """

    Parameters
    ----------
    table : pandas.DataFrame
        Table of event ids, primary and secondary sites, rho & delta values, and nearest neighbors.
    iters : int, optional
        Number of times to iterate over unassigned clusters searching for assigned nearest neighbors.
    """
    last_count = -1

    for i in range(iters):
        for cid in table.computed_cluster.unique():
            if cid <= 0:
                continue
            event_ids = table[table.computed_cluster == cid].index.values
            unassigned = table[(table.computed_cluster == -1) & (table.computed_nearest_neighbor.isin(event_ids))]
            table.loc[unassigned.index, "computed_cluster"] = cid

        count_unassigned = table[table.computed_cluster == -1].shape[0]
        if count_unassigned == last_count or count_unassigned == 0:
            break

        last_count = count_unassigned

    if count_unassigned > 0:
        warnings.warn("nearest-neighbor loop(s) encountered; not all events assigned to clusters")


def build_confusion_matrix(table, sort=True):
    """

    Parameters
    ----------
    table : pandas.DataFrame
    sort : bool, optional
        Sort the output by best match (naively, for now).

    Returns
    -------
    confusion_matrix : pandas.DataFrame

    """

    assert isinstance(table, pd.DataFrame)

    jrc_labels = table.auto_cluster.values.copy()
    computed_labels = table.computed_cluster.values.copy()

    jrc_unique = np.unique(jrc_labels)
    comp_unique = np.unique(computed_labels)

    confusion_matrix = np.zeros((jrc_unique.size, comp_unique.size), dtype=np.int64)

    for i, jrc_label in enumerate(jrc_unique):
        mask = jrc_labels == jrc_label
        comp_matches = computed_labels[mask]

        for j, comp_label in enumerate(comp_unique):
            confusion_matrix[i, j] = np.count_nonzero(comp_matches == comp_label)

    if sort:
        for i in range(jrc_unique.size):
            row = confusion_matrix[i, i:]
            sort_indices = np.flipud(np.argsort(row))

            confusion_matrix[:, i:] = confusion_matrix[:, i + sort_indices]
            comp_unique[i:] = comp_unique[i + sort_indices]

    # eliminate columns where all rows are zero
    keep = ~np.all(confusion_matrix == 0, axis=0)
    confusion_matrix = confusion_matrix[:, keep]
    comp_unique = comp_unique[keep]

    confusion_matrix = pd.DataFrame(confusion_matrix, index=jrc_unique, columns=comp_unique)

    confusion_matrix.columns.name = "computed label"
    confusion_matrix.index.name = "JRC label"

    return confusion_matrix
